package com.schedule.mmt.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Flight {
    String flightCode;
    String fromAirport;
    String toAirport;
    String departureTime;
    String arrivalTime;
}
