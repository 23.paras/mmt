package com.schedule.mmt.model;

import lombok.Data;

@Data
public class FlighDetails implements Comparable<FlighDetails>{

    private String cities;
    private String flightCodes;
    int time;

    @Override
    public int compareTo(FlighDetails flighDetails) {
        if(time==flighDetails.getTime())
            return 0;
        else if(time>flighDetails.getTime())
            return 1;
        else
            return -1;
    }
}