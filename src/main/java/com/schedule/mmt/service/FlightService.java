package com.schedule.mmt.service;

import org.json.JSONArray;
import org.springframework.stereotype.Service;

@Service
public interface FlightService {

    public String getFlightsDetails(String fromAirport , String toAirport);
}
