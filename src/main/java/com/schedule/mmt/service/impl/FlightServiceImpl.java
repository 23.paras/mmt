package com.schedule.mmt.service.impl;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.schedule.mmt.model.FlighDetails;
import com.schedule.mmt.model.Flight;
import com.schedule.mmt.service.FlightService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.*;

@Component
@Slf4j
public class FlightServiceImpl implements FlightService {
    public static Map<String, Set<String>> dfsMap = new HashMap<>();
    public static Map<String, List<Flight>> city_cityToFlights = new HashMap<>();
    public static List<String> listDfsResult = new ArrayList<>();

    //parse csv
    static {
        String fileName = "src/main/resources/ivtest-sched.csv";
        try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
            List<String[]> rows = reader.readAll();
            for (String[] row : rows) {
                if (row[3].length() < 4) {
                    row[3] = "0" + row[3];
                }
                if (row[4].length() < 4) {
                    row[4] = "0" + row[4];
                }
                Flight flight = new Flight(row[0], row[1], row[2], row[3], row[4]);

                if (city_cityToFlights.containsKey(row[1] + "_" + row[2])) {
                    List<Flight> fl = city_cityToFlights.get(row[1] + "_" + row[2]);
                    fl.add(flight);
                    city_cityToFlights.put(row[1] + "_" + row[2], fl);
                } else {
                    List<Flight> fl = new ArrayList<>();
                    fl.add(flight);
                    city_cityToFlights.put(row[1] + "_" + row[2], fl);
                }

                if (dfsMap.containsKey(row[1])) {
                    Set<String> fl = dfsMap.get(row[1]);
                    fl.add(row[2]);
                    dfsMap.put(row[1], fl);
                } else {
                    Set<String> fl = new HashSet<>();
                    fl.add(row[2]);
                    dfsMap.put(row[1], fl);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFlightsDetails(String fromAirport, String toAirport) {
        findPathUsingDfs(fromAirport, toAirport);
        JSONArray jsonArray = getFlightsData();
        return jsonArray.toString();
    }


    private void findPathUsingDfs(String s, String d) {
        Map<String, Boolean> isVisited = new HashMap<>();
        listDfsResult.clear();
        for (String sd : dfsMap.keySet()) {
            isVisited.put(sd, false);
        }

        ArrayList<String> pathList = new ArrayList<>();

        pathList.add(s);

        findPathUsingDfsUtil(s, d, isVisited, pathList);
    }

    private void findPathUsingDfsUtil(String u, String d,
                                      Map<String, Boolean> isVisited,
                                      List<String> localPathList) {

        if (u.equals(d)) {
            String s = String.join("_", localPathList);
            ;

            listDfsResult.add(s);
            // if match found then no need to traverse more till depth
            return;
        }
        if (localPathList.size() >= 3) {
            return;
        }
        // Mark the current node
        isVisited.put(u, true);


        for (String i : dfsMap.get(u)) {
            if (!isVisited.get(i)) {
                // store current node
                // in path[]
                localPathList.add(i);
                findPathUsingDfsUtil(i, d, isVisited, localPathList);

                // remove current node
                // in path[]
                localPathList.remove(i);
            }
        }
        //  isVisited.put(u,false) ;
        // Mark the current node
        // isVisited[u] = false;
    }

    private JSONArray getFlightsData() {
        Set<FlighDetails> oneStopFlightDetails = new TreeSet<>();
        Set<FlighDetails> directFlightDetails = new TreeSet<>();

        for (String cityList : listDfsResult) {
            String[] cities = cityList.split("_");

            if (cities.length > 2) {
                String city1 = cities[0] + "_" + cities[1];
                String city2 = cities[1] + "_" + cities[2];


                List<Flight> flights = city_cityToFlights.get(city1);

                int timeTaken = 0;
                for (Flight flight : flights) {
                    LocalTime departure = LocalTime.of(Integer.parseInt(flight.getDepartureTime().substring(0, 2)),
                            Integer.parseInt(flight.getDepartureTime().substring(2)));
                    LocalTime arrival = LocalTime.of(Integer.parseInt(flight.getArrivalTime().substring(0, 2)),
                            Integer.parseInt(flight.getArrivalTime().substring(2)));
                    if (departure.isAfter(arrival)) {
                        timeTaken = (24 - departure.getHour() + arrival.getHour()) * 60 - departure.getMinute() + arrival.getMinute();
                    } else {
                        timeTaken = (arrival.getHour() - departure.getHour()) * 60 + (arrival.getMinute() - departure.getMinute());
                    }
                    arrival = arrival.plusHours(2l);
                    timeTaken = timeTaken + 120;
                    List<Flight> flights2 = city_cityToFlights.get(city2);
                    for (Flight flight1 : flights2) {
                        int timeTaken2 = 0;
                        LocalTime departure2 = LocalTime.of(Integer.parseInt(flight1.getDepartureTime().substring(0, 2)),
                                Integer.parseInt(flight1.getDepartureTime().substring(2)));
                        LocalTime arrival2 = LocalTime.of(Integer.parseInt(flight1.getArrivalTime().substring(0, 2)),
                                Integer.parseInt(flight1.getArrivalTime().substring(2)));
                        if (arrival.equals(departure2)) {
                            continue;
                        }
                        if (arrival.isAfter(departure2)) {
                            timeTaken2 = timeTaken2 + (24 - arrival.getHour() + departure2.getHour()) * 60 - arrival.getMinute() + departure2.getMinute();
                        } else {
                            timeTaken2 = timeTaken2 + (departure2.getHour() - arrival.getHour()) * 60 + (departure2.getMinute() - arrival.getMinute());
                        }
                        if (departure2.isAfter(arrival2)) {
                            timeTaken2 = timeTaken2 + (24 - departure2.getHour() + arrival2.getHour()) * 60 - departure2.getMinute() + arrival2.getMinute();
                        } else {
                            timeTaken2 = timeTaken2 + (arrival2.getHour() - departure2.getHour()) * 60 + (arrival2.getMinute() - departure2.getMinute());
                        }

                        FlighDetails flighDetails = new FlighDetails();
                        flighDetails.setCities(cityList);
                        flighDetails.setTime(timeTaken + timeTaken2);
                        flighDetails.setFlightCodes(flight.getFlightCode() + "_" + flight1.getFlightCode());
                        oneStopFlightDetails.add(flighDetails);


                    }


                }
            } else {
                List<Flight> flights = city_cityToFlights.get(cityList);
                int timeTaken = 0;
                for (Flight flight : flights) {
                    LocalTime departure = LocalTime.of(Integer.parseInt(flight.getDepartureTime().substring(0, 2)),
                            Integer.parseInt(flight.getDepartureTime().substring(2)));
                    LocalTime arrival = LocalTime.of(Integer.parseInt(flight.getArrivalTime().substring(0, 2)),
                            Integer.parseInt(flight.getArrivalTime().substring(2)));
                    if (departure.isAfter(arrival)) {
                        timeTaken = (24 - departure.getHour() + arrival.getHour()) * 60 - departure.getMinute() + arrival.getMinute();
                    } else {
                        timeTaken = (arrival.getHour() - departure.getHour()) * 60 + (arrival.getMinute() - departure.getMinute());
                    }
                    FlighDetails flighDetails = new FlighDetails();
                    flighDetails.setCities(cityList);
                    flighDetails.setTime(timeTaken);
                    flighDetails.setFlightCodes(flight.getFlightCode());
                    directFlightDetails.add(flighDetails);
                }
            }
        }
        List<FlighDetails> list = new LinkedList<>();
        for (FlighDetails flighDetails : directFlightDetails) {
            list.add(flighDetails);
            if (list.size() == 5) {
                break;
            }
        }
        Map<String, FlighDetails> map = new LinkedHashMap<>();
        int size = 5 - list.size();
        for (FlighDetails f1 : oneStopFlightDetails) {
            if (map.size() == size) {
                break;
            }
            if (!map.containsKey(f1.getCities())) {
                map.put(f1.getCities(), f1);
            }
        }
        for (String cities : map.keySet()) {
            list.add(map.get(cities));
        }

        JSONArray jsonArray = new JSONArray();
        for (FlighDetails f1 : list) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(f1.getFlightCodes(), f1.getTime());
            JSONObject jsonObject2 = new JSONObject();
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put(f1.getCities(), jsonObject);
            jsonArray.put(jsonObject1);


        }

        return jsonArray;
    }
}
