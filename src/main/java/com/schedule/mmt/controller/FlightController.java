package com.schedule.mmt.controller;

import com.schedule.mmt.response.ResponseDTO;
import com.schedule.mmt.service.FlightService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class FlightController {

    @Autowired
    FlightService flightService;

    @GetMapping(value = "flights/{fromAirport}/{toAirport}",produces = "application/json")
    public ResponseEntity<String> getMerchantConfigs(@PathVariable String fromAirport, @PathVariable String toAirport) {
        log.debug("MerchantConfigController.updateMerchantConfig begins");
        return new ResponseEntity<>(flightService.getFlightsDetails(fromAirport,toAirport), HttpStatus.OK);
    }
}
