package com.schedule.mmt;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class MmtApplicationTests {
    @Autowired
    private MockMvc mvc;

    @Test
    void contextLoads() {
    }

    @Test
    public void atqT0Blr() throws Exception {
        mvc.perform(get("/flights/ATQ/BLR")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ORIGIN, "*"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"ATQ_BLR\":{\"6845\":185}},{\"ATQ_DEL_BLR\":{\"2057_819\":365}},{\"ATQ_BOM_BLR\":{\"6261_283\":475}},{\"ATQ_CCU_BLR\":{\"5926_932\":555}},{\"ATQ_PNQ_BLR\":{\"286_954\":1120}}]"))
                .andReturn();
    }

    @Test
    public void ixcToCok() throws Exception {
        mvc.perform(get("/flights/IXC/COK")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ORIGIN, "*"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"IXC_DEL_COK\":{\"2409_2123\":375}},{\"IXC_BLR_COK\":{\"593_463\":410}},{\"IXC_BOM_COK\":{\"264_832\":610}},{\"IXC_CCU_COK\":{\"376_6162\":875}},{\"IXC_MAA_COK\":{\"549_327\":965}}]"))
                .andReturn();
    }

    @Test
    public void ixcToGau() throws Exception {
        mvc.perform(get("/flights/IXC/GAU")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ORIGIN, "*"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"IXC_GAU\":{\"717\":220}},{\"IXC_DEL_GAU\":{\"2152_694\":335}},{\"IXC_CCU_GAU\":{\"376_833\":495}},{\"IXC_BLR_GAU\":{\"591_457\":760}},{\"IXC_HYD_GAU\":{\"615_6538\":795}}]"))
                .andReturn();
    }

	@Test
	public void ixcToPat() throws Exception {
		mvc.perform(get("/flights/IXC/PAT")
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
				.header(HttpHeaders.ORIGIN, "*"))
				.andExpect(status().isOk())
				.andExpect(content().string("[{\"IXC_PAT\":{\"717\":110}},{\"IXC_PAT\":{\"6133\":230}},{\"IXC_DEL_PAT\":{\"2152_107\":290}},{\"IXC_BLR_PAT\":{\"593_792\":485}},{\"IXC_HYD_PAT\":{\"615_154\":1040}}]"))
				.andReturn();
	}

}
